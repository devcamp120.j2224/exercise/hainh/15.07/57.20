public class CircleCylinderClass {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle("green" ,3.0);

        System.out.println( "circle1 : " + circle1.toString() + "\n" + 
                            "circle2 : " + circle2.toString() + "\n" +
                            "circle3 : " + circle3.toString());

        CircleLy circleLy1 = new CircleLy();   
        CircleLy circleLy2 = new CircleLy(2.5);     
        CircleLy circleLy3 = new CircleLy(3.5 ,1.5);                 
        CircleLy circleLy4 = new CircleLy("green" , 3.5 ,1.5);     

        System.out.println( "circleLy1 : " + circleLy1.toString() + "\n" +  
                            "circleLy2 : " + circleLy2.toString() + "\n" +
                            "circleLy3 : " + circleLy3.toString() + "\n" +
                            "circleLy4 : " + circleLy4.toString());                                              
    }
}
