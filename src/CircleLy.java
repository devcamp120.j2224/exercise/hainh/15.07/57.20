public class CircleLy extends Circle {
    double height ;

    public CircleLy() {
     
    }

    public CircleLy(double height) {
        this.height = height;
    }

    public CircleLy(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public CircleLy(String color, double radius, double height) {
        super(color, radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getVolume(){
        return 3.14 * height ;
    }

    
}
