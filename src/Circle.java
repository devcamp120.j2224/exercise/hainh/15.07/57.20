public class Circle {
    String color ;
    double radius = 1.0 ;

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    
    public Circle() {
    }
    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle(String color, double radius) {
        this.color = color;
        this.radius = radius;
    }

    public double getArea(){
        return radius * radius * 3.14 ;
    }

    @Override
    public String toString() {
        return "Circle [color=" + color + ", radius=" + radius + "]";
    }

    
}
